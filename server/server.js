var http = require("http"),
    path = require("path");

var express        = require("express"),
    bodyParser     = require("body-parser"),
    loginRoute     = require("./routes/login"),
    homeRoute      = require("./routes/home"),
    chats          = require("./routes/chats"),
    router         = express(),
    multer         = require("multer"),
    httpSocket     = require("http").Server(express);
    
var server         = http.createServer(router);
var socketIO       = require("socket.io").listen(server);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
router.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//just for debuging
router.use(function (req, res,next) {
    var headers = req.headers;
    var method = req.method;
    var url = req.url;
    var body = req.body;
    console.log(headers + ", " + method + ", " + url + ", " + body);
    next();
});


router.post('/side-menu/home/json/save_location', homeRoute.updatePosition);
router.get('/side-menu/home', homeRoute.findNearbyUsers);
router.get('/side-menu/home/userChat', homeRoute.fetchConversation);
router.post('/side-menu/home', chats.storeNewMessage);
router.post('/login', loginRoute.checkLoginCredentials);
router.post('/signup', loginRoute.checkSignupEmail);
router.post('/activation_flow', loginRoute.addProfileInfo);
router.get('/side-menu/chats/:nickname', chats.displayChatInfo);
router.get('/side-menu/topics', chats.getAllTopics);
router.post('/side-menu/topics', chats.createNewTopic);
router.get('/side-menu/topics/:topicId', chats.getMessagesOfTopic);
router.post('/side-menu/topics/topicDetails', chats.storeNewPost);
router.post('/side-menu/chats/chatDetails', chats.storeNewMessage);
router.get('/side-menu/settings', loginRoute.removePosition);

//SOCKET Functions that will allow to live update messages and posts for all users
socketIO.on('connection', function(socket) {
   console.log('connected');
   
   socket.on('send message', function (info) {
      console.log(info);
      socketIO.emit('send message', info);
   });
   
   socket.on('new topic', function (info) {
      console.log(info);
      socketIO.emit('new topic', info);
   });
   
   socket.on('new post', function (info) {
       console.log(info);
       socketIO.emit('new post', info);
   })
});



//image is uploaded from client side
var storage    =   multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './images');
      },
      filename: function (req, file, callback) {
          console.log("eimai sto filename, ");
          console.log(file.fieldname);
        callback(null, file.originalname + '.png'); 
        // + Date.now());
      }
    });
    
var upload = multer({storage : storage}).single('vlasis');
router.post('/uploadImage', function(req, res, next) {
    upload(req, res, function(err) {
       if (err) {
           console.log("Error: " + err);
           return res.end("error uploading file");
       } 
       console.log(req.body.vlasis);
       console.log("File uploaded");
       res.end("file is uploaded");
       console.log(req.file);
    });
    console.log("mou hr8e h eikona");
    
});

router.get('/getImage', function (req, res, next) {
    console.log('mpika sto download');
    var avatar = req.query.image;
    console.log(req.query.image);
    console.log(avatar);
    res.sendFile('/home/ubuntu/workspace/images/' + avatar);
});

//server start listening
server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  process.setMaxListeners(10);
  console.log("Server listening at", addr.address + ":" + addr.port);
});