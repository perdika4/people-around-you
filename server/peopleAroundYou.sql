-- This file is used to set up the mySQL database.
-- When the file is ran, all tables are destroyed and created again.

set foreign_key_checks = 0;

DROP TABLE IF EXISTS Profile;
DROP TABLE IF EXISTS Position;
DROP TABLE IF EXISTS Languages;
DROP TABLE IF EXISTS Topic;
DROP TABLE IF EXISTS Settings;
DROP TABLE IF EXISTS TopicMessage;
DROP TABLE IF EXISTS Conversation;
DROP TABLE IF EXISTS Private_message;


set foreign_key_checks = 1;
SET time_zone = "+01:00";



CREATE TABLE Profile (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NULL,
  surname VARCHAR(20) NULL,
  nickname VARCHAR(30) NOT NULL UNIQUE,
  email VARCHAR(50) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  date_of_birth DATE NULL,
  status VARCHAR(60) NULL,
  avatar VARCHAR(34) NOT NULL, 
  phone CHAR(11) NULL,
  language VARCHAR(60) NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE Position (
	person VARCHAR(30) NOT NULL,
	latitude DOUBLE(10,8) NULL,
	longitude DOUBLE(11,8) NULL,
	FOREIGN KEY (person) REFERENCES Profile(nickname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE Settings(
	person VARCHAR(30) NOT NULL,
	showOnline ENUM('true', 'false') NOT NULL DEFAULT 'true',
	showNickname ENUM('true', 'false') NOT NULL DEFAULT 'true',
	showStatus ENUM('true', 'false') NOT NULL DEFAULT 'true',
	showProfileInfo ENUM('true', 'false') NOT NULL DEFAULT 'true',
	radarRadius SMALLINT NOT NULL DEFAULT 250,
	frameRate VARCHAR(10) NOT NULL,
	FOREIGN KEY (person) REFERENCES Profile(nickname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE Topic(
	topicId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	author VARCHAR(30) NOT NULL,
	title VARCHAR (50) NOT NULL,
	FOREIGN KEY (author) REFERENCES Profile(nickname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE TopicMessage(
	tmID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	author VARCHAR(30) NOT NULL,
	inTopic INT NOT NULL,
	message VARCHAR(255) NOT NULL,
	time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (author) REFERENCES Profile(nickname),
	FOREIGN KEY (inTopic) REFERENCES Topic(topicId)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE Conversation (
	convId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	participantA VARCHAR(30) NOT NULL,
	participantB VARCHAR(30) NOT NULL,
	FOREIGN KEY (participantA) REFERENCES Profile(nickname),
	FOREIGN KEY (participantB) REFERENCES Profile(nickname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE Private_message(
	pmID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	conversation INT NOT NULL,
	author VARCHAR(30) NOT NULL,
	message VARCHAR(255) NOT NULL,
	time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (author) REFERENCES Profile(nickname),
	FOREIGN KEY (conversation) REFERENCES Conversation(convId)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;



