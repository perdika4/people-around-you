exports.isConnectionSuccess = function(err, res) {
    if (err) {
        res.json({"code: ": 100, "status" : "Error in connection."});
        return false;
    }
    else{
        console.log("connected to database");
        return true;
    }
};


exports.connectionWithDatabase = function(connection, res) {
    return connectWithDatabase(connection, res);
};

exports.insertQuery = function(query, values, connection, res) {
    connection.query(query, values, function(err, rows, fields) {
        if (err) { 
            console.log("error when performing the insert query: " + err); 
            res.end(JSON.stringify({answer: "fail"}));
        }
        else {
            console.log("insertion success.");
            res.end(JSON.stringify({answer: "inserted"}));
        }
     if (connectWithDatabase(connection, res) ) { 
         return; 
     }   
    });
};

function connectWithDatabase(connection, res) {
    connection.on('error', function(err){ 
        console.log(err);
        res.json({"code: " : 100, "status: " : "Error in connection with database."});
        return true;
    });
    return false;
}

