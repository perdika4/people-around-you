var pool   = require("../database");
var shared = require("./sharedFunctions");


// var sessions = [
//     {id:0 , title:"Introduction to Ionic", speaker:"CHRISTOPHE COENRAETS", time:"9:40am", room:"Ballroom A", description: "In this session, you'll learn how to build a native-like mobile application using the Ionic Framework, AngularJS, and Cordova."},
//     {id:1 , title:"AngularJS in 50 Minutes", speaker:"LISA SMITH", time:"10:10am", room:"Ballroom B", description: "In this session, you'll learn everything you need to know to start building next-gen JavaScript applications using AngularJS."},
//     {id:2 , title:"Contributing to Apache Cordova", speaker:"JOHN SMITH", time:"11:10am", room:"Ballroom A", description: "In this session, John will tell you all you need to know to start contributing to Apache Cordova and become an Open Source Rock Star."},
//     {id:3 , title:"Mobile Performance Techniques", speaker:"JESSICA WONG", time:"3:10Pm", room:"Ballroom B", description: "In this session, you will learn performance techniques to speed up your mobile application."},
//     {id:4 , title:"Building Modular Applications", speaker:"LAURA TAYLOR", time:"2:00pm", room:"Ballroom A", description: "Join Laura to learn different approaches to build modular JavaScript applications."}
// ];

// exports.findAll = function (req, res, next) {
//     console.log("mpika sto find all");
//     res.send(sessions);
// };



exports.displayChatInfo = function (req, res, next) {
    console.log("mpika sto chat info");
    pool.getConnection( function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) {
            console.log('den petuxe to display chat info: ' + err);
            return;
        }
        console.log('to param einai: ');
        console.log('o user einai: ' + req.params.nickname);
        console.log('o owner einai: ' + req.query.owner);
        var user = req.params.nickname;
        var owner = req.query.owner;
        var query = "SELECT message, author, time, conversation " +
                    "FROM Private_message " +
                    "INNER JOIN Conversation ON convId = conversation " +
                    "WHERE (participantA = ? AND participantB = ?) " +
                    "OR (participantA = ? AND participantB = ?) " +
                    "ORDER BY time";
        var placeholders = [user, owner, owner, user];
        var result = connection.query(query, placeholders, function(err, rows, fields) {
            if (err) {
                console.log("error with performing the query in displayChatInfo select message");
                console.log(err);
            }
            else {
                res.end(JSON.stringify(rows));
                console.log("sent the messages");
            }
        });
      if (shared.connectionWithDatabase(connection, res) ) {
          connection.release();
          return; 
      }
      connection.release();
    });
};

exports.getAllTopics = function(req, res, next) {
    console.log('mphka sto get all topics');
    pool.getConnection(function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to get all topics: ' + err);
            return; 
        }
        var query = "SELECT * FROM Topic;";
        var result = connection.query(query, function(err, rows, fields) {
           if (err) {
               console.log('error with performing the query in select from topics');
               console.log(err);
           } 
           else{
               res.end(JSON.stringify(rows));
               console.log('sent topics');
           }
        });
        if (shared.connectionWithDatabase(connection, res) ) { 
            connection.release();
            return; 
        }
        connection.release();
    });
};

exports.createNewTopic = function(req, res, next) {
    console.log('mpika sto create topic');
    pool.getConnection(function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to create new topic: ' + err);
            return; 
        }
        var author = req.body.author;
        var title = req.body.title;
        console.log(author);
        var values = {author: author, title: title};
        var query = "INSERT INTO Topic SET ?";
        shared.insertQuery(query, values, connection, res);
        res.end(JSON.stringify({answer: 'inserted'}));
    });
};

exports.getMessagesOfTopic = function(req, res, next) {
    console.log('mpika sto getMessagesOfTopic');
    pool.getConnection(function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to get messages of topic: ' + err);
            return; 
        }
        var id = req.params.topicId;
        console.log(id);
        var query = "SELECT author, message " +
                    "FROM TopicMessage " +
                    "WHERE inTopic = ? " +
                    "ORDER BY time";
        var placeholders = [id];
        var result = connection.query(query, placeholders, function(err, rows, fields) {
        if (err) {
                console.log("error with performing the query in get messages from topic");
                console.log(err);
            }
            else {
                res.end(JSON.stringify(rows));
                console.log("sent the posts from the topic");
            }
        });
        if (shared.connectionWithDatabase(connection, res) ) { 
            connection.release();
            return; 
        }
        connection.release();
    });
};

exports.storeNewPost = function(req, res, next) {
    console.log('mphka sto storeNewPost');
     pool.getConnection(function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to store new post: ' + err);
            return; 
        }
        var author = req.body.author; 
        var text = req.body.text;
        var topicId = req.body.topicId;
        var values = {author: author, message: text, inTopic: topicId};
        var query = "INSERT INTO TopicMessage SET ?";
        shared.insertQuery(query, values, connection, res);
    });
}

exports.storeNewMessage = function(req, res, next) {
    console.log('mphka sto storeNewMessage');
    pool.getConnection(function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to store new message: ' + err);
            return; 
        }
        var author = req.body.author; 
        var message = req.body.message;
        var user = req.body.user;
        var conversation = req.body.conversation;
        console.log(req.body.author + ', ' + req.body.message + ', ' + req.body.user + ', ' + conversation);
        // a conversation between these 2 users already exists
        if(conversation != -1) {
            var values = {author: author, message: message, conversation: conversation };    
            var query = "INSERT INTO Private_message SET ?";
            insertInExistingConversation(query, values, connection, res, conversation);
        }
        else{ //need to create a new conversation first
            values = {participantA: author,  participantB: user};
            query = "INSERT INTO Conversation SET ?"
            connection.query(query, values, function(err, rows, fields) {
               if(err) {
                    console.log("error with performing the query for creating new conversation: " + err);
                    res.end(JSON.stringify({answer: "fail"}));
               } 
               else{
                   getTheConversationNumber(res, connection, author, user, message);
               }
            });
        }
        if (shared.connectionWithDatabase(connection, res) ) { 
            connection.release();
            return; 
        }
        connection.release();
    });
};

function insertInExistingConversation(query, values, connection, res, conversation) {
        connection.query(query, values, function(err, rows, fields) {
        if (err) { 
            console.log("error when performing the insert query: " + err); 
            res.end(JSON.stringify({answer: "fail", conversation: conversation}));
        }
        else {
            console.log("insertion success.");
            res.end(JSON.stringify({answer: "inserted", conversation: conversation}));
        }
       if (shared.connectionWithDatabase(connection, res) ) { return; }
    });
}

    /*get the id of the conversation. I could retrieve it and use it directly
      in the insert quert, but I want to value to send it back to the response*/
function getTheConversationNumber(res, connection,  author, user, message) {
    var placeholders = [author, user];  
    var query = "SELECT convId FROM Conversation WHERE participantA = ? and participantB = ? ";  
    var result = connection.query(query, placeholders, function(err, rows, fields) {
       if(err) {
           console.log("error with performing the query in select convId: " + err);
           res.end(JSON.stringify({answer: "fail"}));
       } 
       else {
           console.log('the result is: ' + rows[0] + ', ' + rows[0].convId);
           insertMessageToNewlyCreatedConversation(res, connection, author, user, message, rows[0].convId);
       }
       if (shared.connectionWithDatabase(connection, res) ) { return; }
    });
}

function insertMessageToNewlyCreatedConversation(res, connection, author, user, message, conversation) {
    var placeholders = [author, message, conversation] ;
    var query = "INSERT INTO Private_message (author, message, conversation) " +
                        "VALUES (?, ?, ?)";
    connection.query(query, placeholders, function(err, rows, fields) {
            if (err) { 
                console.log("error when performing the insert new message to new Conversation query: " + err); 
                res.end(JSON.stringify({answer: "fail"}));
            }
            else {
                console.log("insertion success.");
                res.end(JSON.stringify({answer: "inserted", conversation: conversation}));
            }
       if (shared.connectionWithDatabase(connection, res) ) { return; }
    });    
}