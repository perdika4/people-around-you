var pool   = require('../database');
var shared = require("./sharedFunctions");

exports.updatePosition = function(req, res, next) {
    pool.getConnection( function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) {
            console.log('den petuxe to update position: ' + err);
            return; 
        }
        var lati = req.body.lati;
        var longi = req.body.longi;
        var nickname = req.body.nickname;
        console.log(lati + ", " + longi + ", " + nickname);
        var query = "UPDATE Position SET latitude = ? , longitude = ? WHERE person = ?";
        connection.query(query, [lati, longi, nickname] , function(err, rows, fields) {
            if (err) {
                console.log("error with performing the query in update position");
            }
            else{
                console.log(lati + ", " + longi + ", " + nickname);        
                console.log("update position success");
                res.end(JSON.stringify({answer: "position updated"}));
            }
        if (shared.connectionWithDatabase(connection, res) ) { 
            connection.release();
            return;
        }
        connection.release();
        })
    });
};

exports.fetchConversation = function(req, res, next) {
    console.log('mpika sto fetchConversation');
    pool.getConnection( function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to fetch conversation: ' + err);
            return; 
        }
        var user = req.query.nickname;
        var owner = req.query.owner;
        console.log('owner: ' + owner + ' user: ' + user);
        var query = "SELECT message, author, time, conversation " +
                    "FROM Private_message " +
                    "INNER JOIN Conversation ON convId = conversation " +
                    "WHERE (participantA = ? AND participantB = ?) " +
                    "OR (participantA = ? AND participantB = ?) " +
                    "ORDER BY time";
        var placeholders = [user, owner, owner, user];
        var result = connection.query(query, placeholders, function(err, rows, fields) {
            if (err) {
                console.log("error with performing the query in fetchConversation");
                console.log(err);
            }
            else {
                res.end(JSON.stringify(rows));
                console.log("sent the messages");
            }
        });
      if (shared.connectionWithDatabase(connection, res) ) {
          connection.release();
          return; 
      }
      connection.release();
    });
};

exports.findNearbyUsers = function (req, res, next) {
    var lati = parseFloat(req.query.lati); 
    var longi = parseFloat(req.query.longi);
    var latiDist = parseFloat(req.query.latiDist);
    var longiDist = parseFloat(req.query.longiDist);
    // console.log(lati + ", " + longi + ', ' + latiDist + ', ' + longiDist);
    pool.getConnection( function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) {
            console.log('den petuxe to find nearby users: ' + err);
            return; 
        }
        var placeholders = [(lati + latiDist), (lati - latiDist), (longi + longiDist), (longi - longiDist)];
        var query = "SELECT nickname, latitude, longitude FROM Profile " +
                    "INNER JOIN Position ON nickname = person " +
                    "WHERE latitude < ? AND latitude > ? " +
                    "AND longitude < ? AND longitude > ?;";
        connection.query(query, placeholders, function(err, rows, fields) {
           if (err) {
               console.log('error with performing the query in search nearby users');
           } 
           else {
            //   console.log(rows.length);
            //   console.log(rows[0]);
            //   console.log(JSON.stringify(rows));
               res.end(JSON.stringify(rows));  
              console.log('Sent the list of nearby users');
           }
           if (shared.connectionWithDatabase(connection, res) ) { 
               connection.release();
               return; 
           }
           connection.release();
        });            
    });
};