var pool   = require('../database');
var scrypt = require('js-scrypt');
var shared = require('./sharedFunctions');
var base64 = require("base64-js");


exports.checkLoginCredentials = function(req, res, next) {
    console.log("entered checkLoginCredentials");
    pool.getConnection( function (err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('failed connection sto checkLoginCredentials');
            return; 
        }
        console.log("connected as id: " + connection.threadId);
        var email = req.body.email;
        var password = req.body.password;
        console.log("old: " + password);
        password = hashPassword(password, email);
        console.log("new: " + password);
        var query = "select * from Profile where email = ? and password = ?";
        var result = connection.query(query, [email, password], function(err, rows, fields) {
            if (err) {
                console.log("error with performing the query");
            }
            else {
                console.log(rows.length);
                console.log(rows[0]);
                if(rows.length == 0) {
                    res.end(JSON.stringify({nickname: ""}));
                }
                else {
                    console.log(rows[0]);
                    res.end(JSON.stringify(rows[0]));    
                }
            }
            //RELEASE
            if (shared.connectionWithDatabase(connection, res) ) { 
                connection.release();
                return; 
            }
            connection.release();
        });
    });
};

exports.checkSignupEmail = function(req, res, next) {
    pool.getConnection( function (err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to check signup email: ' + err);
            return; 
        }
        console.log("connected as id: " + connection.threadId);
        var email = req.body.email;
        var password = req.body.password;
        var nickname = req.body.nickname;
        console.log("apo to client: " + email + ", " +password + ", " + nickname);
        var query = "select nickname from Profile where email = ? OR nickname = ?";
        var result = connection.query(query, [email, nickname], function(err, rows, fields) {
            if (err) {
                console.log("error with performing the query");
            }
            else {
                console.log(rows.length);//just for debug
                console.log(rows[0]);
                if(rows.length != 0) {
                    res.end(JSON.stringify({answer: "exists"}));
                    return;
                }
                else {
                    console.log("old: " + password);
                    password = hashPassword(password, email);
                    console.log("new: " + password);
                    var values = { email: email, nickname: nickname, password: password };
                    query = "INSERT INTO Profile SET ?";
                    shared.insertQuery(query, values, connection, res);
                    /* create a row for the user on the Position table with null position
                       this will later update with his actual positions. */
                    values = {person: nickname};
                    query = "INSERT INTO Position SET ?";
                    shared.insertQuery(query, values, connection, res);
                    console.log("Position updated");
                    res.end(JSON.stringify({answer: 'dont exist', email: email, nickname: nickname}));
                    connection.release();
                }
            }
            if (shared.connectionWithDatabase(connection, res) ) { 
                console.log('eimai mesa sto if');
                return; 
            }
            console.log("eimai meta to teleutaio");
        });
    });
};




exports.addProfileInfo = function(req, res, next) {
    pool.getConnection( function (err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to add profile info: ' + err);
            return; 
        }
        console.log("connected as id: " + connection.threadId);
        console.log("auto einai to body: ");
        console.log(req.body);
        var nickname  = getValue(req.body.nickname);
        var firstName = getValue(req.body.firstName);
        var lastName  = getValue(req.body.lastName);
        var status    = getValue(req.body.status);
        var phone     = getValue(req.body.phone);
        var date      = getValue(req.body.date);
        var language  = getValue(req.body.languageSelect);
        var avatar    = nickname + ".png";
        console.log("auto einai to custom pou ekana egw");
        console.log(firstName + ", " + lastName + ", " + status + ", " + phone + ", " + date + ", " + language + ", " + avatar);
        var values = { name: firstName, surname: lastName, date_of_birth: date, status: status, avatar: avatar, phone: phone, language: language };
        var query = "UPDATE Profile SET ? WHERE nickname = ?";
        connection.query(query, [ values, nickname], function(err, rows, fields) {
            if (err) { 
                console.log("error when performing the insert query: " + err); 
                res.end(JSON.stringify({answer: "fail"}));
            }
            else {
                console.log("insertion success.")
                res.end(JSON.stringify({answer: "inserted"}));
            }
            if (shared.connectionWithDatabase(connection, res) ) { 
                connection.release();
                return; 
            }   
            connection.release();
        });
    });
};

exports.removePosition = function( req, res, next) {
    console.log('EIMAI MESA STO REMOVE POSITION!!!!!!!');
    // console.log('eimai mesa sto removePosition');
    pool.getConnection( function(err, connection) {
        if (!shared.isConnectionSuccess(err, res)) { 
            console.log('den petuxe to remove position: ' + err);
            return; 
        }
        var nickname = req.query.nickname;
        var values = {latitude: null, longitude: null};
        var query = "UPDATE Position SET ? WHERE person = ?";
        connection.query(query, [ values, nickname], function(err, rows, fields) {
            if (err) { 
                console.log("error when performing the null positions query " + err); 
                res.end(JSON.stringify({answer: "fail"}));
            }
            else {
                console.log("update success.")
                res.end(JSON.stringify({answer: "updated"}));
            }
            if (shared.connectionWithDatabase(connection, res) ) { 
                connection.release();
                return; 
            }   
            connection.release();
        }); 
    });
};


// SHARED FUNCTIONS
function getValue (value) {
    if (value == undefined) {
        return "";
    }
    return value;
}

function hashPassword(password, email) {
    var resultBuffer = scrypt.hashSync( password, email);
    console.log('result buffer: ' + resultBuffer);
    console.log('typos tou resultBuffer: ' + typeof resultBuffer);
    var result = base64.fromByteArray(resultBuffer);
    console.log('res buf length: ' + resultBuffer.length);
    console.log('result: ' + result);
    return result;
}

function hexToString(array) {
  var result = "";
  for (var i = 0; i < array.length; i++) {
    result += String.fromCharCode(parseInt(array[i], 16));
  }
  return result;
}



