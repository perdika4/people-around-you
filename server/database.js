var mysql= require("mysql");
var pool = mysql.createPool({
   connectionLimit: 100, //important
   host     :'', //had "frog0-people-around-you-3591629" as host name, but it didn't work
   user     :'frog0',
   password :'', 
   database :'peopleAroundYou',
   debug    : false
});

exports.getConnection = function (callback) {
    pool.getConnection(callback);
};