angular.module('app.services', ['ngResource'])

.service('LoginService', function($resource, $ionicPopup) {

	var connectLogin   		= $resource('http://people-around-you-frog0.c9users.io:8080/login');
	var connectSignup  		= $resource('http://people-around-you-frog0.c9users.io:8080/signup');
	var connectProfile 		= $resource('http://people-around-you-frog0.c9users.io:8080/activation_flow');
	var connectHome         = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/home');
	var connectHomeUserChat = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/home/userChat');
	var connectChats        = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/chats/:nickname');
	var connectTopics       = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/topics/:topicId');
	var connectTopicDetails = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/topics/topicDetails');
	var connectChatDetails  = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/chats/chatDetails');
	var connectSettings     = $resource('http://people-around-you-frog0.c9users.io:8080/side-menu/settings');



	//popup message that is used in more than one controllers.
    function alertPopup(popupMessage) {
        var popup = $ionicPopup.alert({
            title: 'Login failed!',
            template: popupMessage
        });
		return popup;
    }

	//store the Nickname in the signup page to use it to name the avatar uploaded in the activation_flow page
	var nicknameStore = window.localStorage;
	function setNickname(nickname) {
		nicknameStore.setItem('nickname' , nickname);
	}

	function getNickname() {
		return nicknameStore.getItem('nickname');
	}


	function starts(s, x) { return s.lastIndexOf(x, 0) === 0; }
	function ends(s, x) { return s.indexOf(x, s.length-x.length) >= 0; }

	return {
		connectLogin   		: connectLogin,
		connectSignup  		: connectSignup,
		connectProfile 		: connectProfile,
		connectHome    		: connectHome,
		connectHomeUserChat : connectHomeUserChat,
		connectChats   		: connectChats,
		connectTopics 		: connectTopics,
		connectTopicDetails : connectTopicDetails,
		connectChatDetails	: connectChatDetails,
		connectSettings     : connectSettings,
		alertPopup     		: alertPopup,
		starts         		: starts,
		ends           		: ends,
		setNickname    		: setNickname,
		getNickname    		: getNickname
	};
})

.service('SharedInfo', [function() {
	var topicTitle;

	return {
	 	topicTitle: topicTitle
	};
}])

.factory('HomeFactory', function($http) {
	return {
		sendLocation: function($params) {
			return $http({
				headers: {'Content-Type': 'application/json'},
			  	url: 'http://people-around-you-frog0.c9users.io:8080/side-menu/home/' + 'json/save_location',
			  	method: "POST",
			  	data: $params
			})
			.success(function(){
		  	});
		}
	};
})

.service('HomeService', [function() {

	//the owner object that will hold all it's profile and settings information
	var OwnerInfo = function() {
		this.lati 	  = null;
		this.longi 	  = null;
		this.email 	  = null;
		this.nickname = null;
		this.avatar   = null;
		this.name 	  = null;
		this.surname  = null;
		this.status   = null;
		this.phone 	  = null;
		this.DOB 	  = null;
		this.language = null;
		this.radius   = 100;
		this.frame    = 'fast';
	};

	var store  = window.localStorage;

	function setOwnerInfo(ownerObj) {
		store.setItem('ownerObj', JSON.stringify({
	    	email: 	  ownerObj.email,
	    	nickname: ownerObj.nickname,
			avatar:   ownerObj.avatar,
			lati: 	  ownerObj.lati,
			longi: 	  ownerObj.longi,
			name: 	  ownerObj.name,
			surname:  ownerObj.surname,
			status:   ownerObj.status,
			phone: 	  ownerObj.phone,
			DOB: 	  ownerObj.DOB,
			language: ownerObj.language,
			radius:   ownerObj.radius,
			frame:    ownerObj.frame
		}));
	}

	function getOwnerInfo() {
		return JSON.parse(store.getItem('ownerObj'));
	}

	var usersList = [];

	var usersHashMap = {};

	return{
		OwnerInfo	 : OwnerInfo,
		setOwnerInfo : setOwnerInfo,
		getOwnerInfo : getOwnerInfo,
		usersList	 : usersList,
		usersHashMap : usersHashMap
	};
}])

.service('servSettings', [function(){

	//	radius functions
	var storage = window.localStorage;
	var radiusValue ;
	function setRadius(data) {
		radiusValue = data.toString();
		storage.setItem('radius', radiusValue);
	 }
	 function getRadius() {
		 return storage.getItem('radius');
	 }

	 //frane rate speed functions
	 function setSpeed(speed) {
		 storage.setItem('speed', speed);
	 }
	 function getSpeed() {
		 return storage.getItem('speed');
	 }

	//  return everything
	 return {
		  setRadius: setRadius,
		  getRadius: getRadius,
		  setSpeed: setSpeed,
		  getSpeed: getSpeed
	 };
}]);
