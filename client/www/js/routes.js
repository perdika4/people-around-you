angular.module('app.routes', [])

.config(function( $stateProvider, $urlRouterProvider, $httpProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



  .state('menu.home', {
    // cache: false,
    url: '/home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('menu.profile', {
    url: '/profile',
    views: {
      'side-menu21': {
        templateUrl: 'templates/profile.html',
        controller: 'profileCtrl'
      }
    }
  })

  .state('menu', {
    // cache: false,
    url: '/side-menu',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl',
    abstract:true
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('menu.profilePhoto', {
    // cache: false,
    url: '/profilePhoto',
    views: {
      'side-menu21': {
        templateUrl: 'templates/profilePhoto.html',
        controller: 'profilePhotoCtrl'
      }
    }
  })

  .state('menu.settings', {
    // cache: false,
    url: '/settings',
    views: {
      'side-menu21': {
        templateUrl: 'templates/settings.html',
        controller: 'settingsCtrl'
      }
    }
  })

  .state('menu.about', {
    url: '/about',
    views: {
      'side-menu21': {
        templateUrl: 'templates/about.html'
      }
    }
  })

  .state('menu.chats', {
    url: '/chats',
    views: {
      'side-menu21': {
        templateUrl: 'templates/chats.html',
        controller: 'chatsCtrl'
      }
    }
  })

  .state('menu.topics', {
      url: '/topics',
      views: {
          'side-menu21' : {
              templateUrl: 'templates/topics.html',
              controller: 'topicsCtrl'
          }
      }
  })

  .state('menu.topicDetails', {
      url: '/topics/:topicId',
      views: {
          'side-menu21' : {
              templateUrl: 'templates/topicDetails.html',
              controller: 'topicDetails'
          }
      }
  })

  .state('menu.chatDetails', {
    url: '/chats/:nickname',
    views: {
      'side-menu21': {
        templateUrl: 'templates/chatDetails.html',
        controller: 'chatDetailsCtrl'
      }
    }
  })

  .state('menu.newPassword', {
    url: '/new_password',
    views: {
      'side-menu21': {
        templateUrl: 'templates/newPassword.html',
        controller: 'newPasswordCtrl'
      }
    }
})

  .state('activationFlow', {
    url: '/activation_flow',
    templateUrl: 'templates/activationFlow.html',
    controller: 'activationFlowCtrl'
});

// $urlRouterProvider.otherwise('/side-menu/home');
$urlRouterProvider.otherwise('/login');




});
