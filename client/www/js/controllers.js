angular.module('app.controllers', ['app.services'])

.controller('homeCtrl', function($scope, HomeFactory, $window, HomeService, $ionicLoading,
                                 $timeout, $ionicScrollDelegate, LoginService, $ionicPopup, $cordovaFileTransfer) {


    //load the service that contains the user's information that will be used throughout the session
    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();

    // will use this service to save messages to the database
    $scope.data = new LoginService.connectHome();

    /*so if I send e message withouth selecting a user not to crash the app
      See function "sendMessage()"*/
    $scope.data.conversation = -2;

    /*socket that will allow to live update incoming messages (socket.io)
     http://socket.io/download/ */
    var socket = io("http://people-around-you-frog0.c9users.io:8080");
    socket.on('send message', function(info) {
        console.log('autoo einai to return apo to socket: ' + info.author + ', ' + info.message);
        $scope.$apply(function() {
            $scope.messages.push(info);
            $timeout(function () {
                messageDiv.scrollTop = messageDiv.scrollHeight;
                viewScroll.scrollBottom();
            }, 300);
        });
    });

    $scope.owner = ownerInfo;
    //take control of the view's scroll functionality
    var viewScroll = $ionicScrollDelegate.$getByHandle('homeScroll');

    //DRAW THE RADAR---------------------------------
    var CANVAS_SIDE = (screen.width - 10);
    var USER_SIDE = 28;
    var RADAR_RADIUS = 100;//default radius
    var FRAME_RATE = 33;//default frame rate
    /*I HARD calculated these values with my calculator based on the RULE OF THREE.
      Work best near England lati and longi. Not to be used near the poles or equator.
      These values are send to the server to filter the database search.*/
    var METERS_TO_LATI = 0.0000089932;
    var METERS_TO_LONGI = 0.0000144359;

    function setFrameRate(value) {
        if(value.localeCompare('slow') === 0) {
            return 1000;
        }
        else if(value.localeCompare('medium') === 0) {
            return 500;
        }
        else if (value.localeCompare('fast') === 0) {
            return 33;
        }
    }


    var drawUpdate = setInterval(function() {
        drawImages();
        printScreenInfo();
    }, FRAME_RATE);

    //function that is called every time the page is on focus.
    $scope.$on('$ionicView.enter', function(event, data) {
        clearInterval(drawUpdate);
        var temp = new HomeService.OwnerInfo();
        temp = HomeService.getOwnerInfo();
        RADAR_RADIUS = ownerInfo.radius;
        FRAME_RATE = setFrameRate(ownerInfo.frame);
        $scope.frame = FRAME_RATE;
        $scope.frameName = ownerInfo.frame;
        $scope.radius = RADAR_RADIUS;
        //update images with new frame rate
        drawUpdate = setInterval(function() {
            drawImages();
            printScreenInfo();
        }, FRAME_RATE);
    });

    // THE PHOTO OBJECT
    var Photo = function (filename, x, y, side, nickname) {
        this.x = x;
        this.y = y;
        this.nickname = nickname;
        this.drawX = this.x;
        this.drawY = this.y;
        this.side = side;
        this.centerX = this.x + (this.side/2);
        this.centerY = this.y + (this.side/2);
        this.lati = null;
        this.longi = null;
        this.image = null;
        //cartesian coordinates AKA with (0,0) the center of the radar
        this.cartX = this.centerX - (CANVAS_SIDE/2);
        this.cartY = (CANVAS_SIDE/2) - this.centerY;
        this.cartHypotenuse = Math.sqrt((this.cartX * this.cartX) + (this.cartY * this.cartY));
        this.cartesianAngle = angleOfPoint(this.centerX, this.centerY);
        if (filename !== undefined && filename !== '' && filename !== null){
            this.image = new Image();
            this.image.src = filename;
        }
        else {
            console.log('unable to load sprite');
        }
        this.draw = function() {
            /*this function is also a constructor. So if I don't
              want a created photo object to print I give it x & y = 1*/
            if(this.x !== 1 && this.y !== 1){
                ctx.drawImage(this.image, this.drawX, this.drawY, this.side, this.side);
            }
        };
    };

    $scope.message = LoginService.getNickname

    function angleOfPoint(x, y) {
        /*if (0,0) is the center of the radar and not the top left corner then
          I need to change the x and y accordingly*/
        var cartX = x - (CANVAS_SIDE/2);
        var cartY = (CANVAS_SIDE/2) -y;
        var hypotenuse = Math.sqrt((cartX * cartX) + (cartY * cartY));
        /*the angle C opposite to the side cartY based to the Law of Cosines is:
          c^2 = a^2 + b^2 - 2*a*b *cos(C) =>
          C = arccos((a^2 + b^2 - c^2) / (2*a*b))*/
        var angle = Math.acos( (Math.pow(cartX, 2) + Math.pow(hypotenuse, 2) - Math.pow(cartY, 2)) / (2 * cartX * hypotenuse) );
        // convert radians to degrees
        angle = radiansToDegrees(angle);
        //to have a range 0-360 instead of 0-180 and 0-180
        if(cartY < 0) {
            angle = 180 + (180 - angle);
        }
        return angle;
    }

	var ownerX = (CANVAS_SIDE/2) - (USER_SIDE/2);
	var ownerY = ownerX;
    //CREATE CANVAS
	$scope.screenSize = screen.width;
	$scope.canvasSides = CANVAS_SIDE;
	var element = document.getElementById("canvas");
	element.innerHTML = '<canvas id="myCanvas" width="' + CANVAS_SIDE + '" height="' + CANVAS_SIDE + '"  style="border:0px solid #B0171F;">' +
							'Your browser does not support the HTML5 canvas tag.' +
						'</canvas>';

    //SET HEIGHT FOR MESSAGING div in home page based on phone's screen height
    var messageDiv = document.getElementById('messagingArea');
    messageDiv.setAttribute("style","height:" + (screen.height - screen.width ) + "px");

    //CREATE AND DISPLAY THE PHOTO IMAGES ON THE CANVAS
	var ctx = document.getElementById("myCanvas").getContext("2d");
    var background = new Photo("img/background.png", 0, 0, CANVAS_SIDE, 'background');
    //HashMap that will store avatars like: 'nickname' : 'imagePath'
    var usersAvatars = {};
    var users = []; //this will hold the nearby users
    var owner = new Photo('img/Red_Arrow.png', ownerX, ownerY, USER_SIDE, 'owner');
    var images = [background, owner]; /// the key
    var count = images.length;
    owner.centerX = CANVAS_SIDE/2;
    owner.centerY = CANVAS_SIDE/2;

    //credit to: K3N for this piece of code to load images with correct order
    //link: http://stackoverflow.com/questions/20317125/html5-canvas-how-to-draw-one-picture-on-top-of-another
    background.image.onload = owner.image.onload = counter;

    /// common loader keeping track if loads
    function counter() {
        count--;
        if (count === 0) {
        	drawImages();
        }
    }

    /// is called when all images are loaded
    function drawImages() {
        if (users.length === 0) {
            drawOnlyUserAndBackground();
        }
        else {
            drawAll();
        }
    }

    function drawOnlyUserAndBackground() {
        console.log('eimsi sto only owner');
        $scope.debug = 'empty list';
        $scope.debug3 = users.length;
        background.draw();
        owner.draw();
    }

    function drawAll() {
        background.draw();
        for (var i = 0; i < users.length; i++) {
            if (users[i].image !== null) {
                if( users[i].cartHypotenuse < CANVAS_SIDE/2) {
                    users[i].draw();
                }
            }
        }
        owner.draw();
    }

    //GET SCREEN TAP COORDINATES
    $scope.addOnClick = function(event) {
        var x = event.offsetX;
        var y = event.offsetY;
        for (var i = 0; i < users.length; i++) {
            if( users[i].drawX <= x && (users[i].drawX + users[i].side) >= x &&
                users[i].drawY <= y && (users[i].drawY + users[i].side) >= y) {
                $scope.data.conversation = users[i].nickname;
                $scope.data.user = users[i].nickname;
                $scope.userAvatar = users[i].image.src;
                var result = LoginService.connectHomeUserChat.query({nickname: users[i].nickname, owner: ownerInfo.nickname});
                result.$promise.then(function(data) {
                    $scope.messages = data;
                    $timeout(function () {
                        messageDiv.scrollTop = messageDiv.scrollHeight;
                    }, 300);
                    if (data.length === 0) {
                        $scope.data.conversation = -1;
                    }
                    else {
                        $scope.data.conversation = data[0].conversation;
                    }
                });
                break;
            }
        }
    	$scope.xi = x;
    	$scope.psi = y;
        var gwnia = angleOfPoint(event.offsetX, event.offsetY);
        $scope.gwnia = gwnia;
    };

  //GET GEOLOCATION AND COMPASS INFORMATION---------------------------
    //geolocation. The user needs to open the GPS
    var timeStamp;
    var geoOptions = {timeout: 4 * 1000, enableHighAccuracy: true };
    $window.navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, geoOptions);
    //compass always works
    var compassOptions = {frequency : 200};
    $window.navigator.compass.getCurrentHeading(headSuccess, headError);
    var headWatchId =  navigator.compass.watchHeading(headSuccess, headError, compassOptions);

	//geolocation functions
   function geolocationSuccess(position){
       //in getCurrentHeading success I want to update the position and call the watchPosition function
       getPositionsCoordinates(position);
       updatePositionsOnDatabase();
    //    startLookingForOtherUsers();
       var geoWatchID = navigator.geolocation.watchPosition(geoWatchSuccess, geoWatchError, geoOptions);
    }

    function geoWatchSuccess(position) {
        getPositionsCoordinates(position);
    }

    function geolocationError(error) {
        $scope.$apply(function() {
            $scope.code = error.code;
            $scope.message = error.message;
            var alertPopup = $ionicPopup.alert({
                title: 'Geolocation Off',
                template: 'Turn on the GPS and Internet connection to locate nearby people',
                okText: 'Got it'
            });
            alertPopup.then(function(res) {
                var Options = {timeout: 12 * 1000, enableHighAccuracy: true };
                $window.navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, Options);
            });
        });
    }

    function geoWatchError(error) {
        $scope.$apply(function() {
            $scope.code = error.code;
            $scope.message = error.message;
        });
    }

    //update the owner's position on the database every one and a half second for other users to see.
    function updatePositionsOnDatabase() {
        setInterval(function() {
            $params = {
                'nickname': ownerInfo.nickname,
                'lati': ownerInfo.lati,
                'longi': ownerInfo.longi
            };
            HomeFactory.sendLocation($params);
            searchNearbyUsers();
        }, 6000);
    }

    function searchNearbyUsers() {
        var ownerInformation = {
            lati: ownerInfo.lati.toString(),
            longi: ownerInfo.longi.toString(),
            latiDist: (RADAR_RADIUS * METERS_TO_LATI).toString(),
            longiDist: (RADAR_RADIUS * METERS_TO_LONGI).toString()
        };
        //get an array of nearby users
        var result = LoginService.connectHome.query(ownerInformation);
        //do something with that data
        result.$promise.then(function(data) {
            $scope.dataLength = (data.length);
            getAllUsersAvatarsFromDatabase(data);
            var index = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].nickname.localeCompare(ownerInfo.nickname) !== 0) {//don't want to add the owner
                    if (data[i].nickname in usersAvatars) {//make sure the images are downloaded
                        var exists = false;
                        /*this is expensive. I should have a hash map instead of a loop to check for active users
                          If it starts to lag I should change the implementation. Also I should remove users that
                          are no longer in radius.*/
                        for (var j = 0; j < users.length; j++) {
                            if ( users[j].image.src.localeCompare(usersAvatars[data[i].nickname]) === 0 ) {
                                exists = true;
                            }
                        }
                        if (exists === false) {
                            users[index] = new Photo(usersAvatars[data[i].nickname], 1, 1, USER_SIDE, data[i].nickname);
                            users[index].lati = data[i].latitude;
                            users[index].longi = data[i].longitude;
                            index++;
                        }
                    }
                }
            }
        });
    }

    function getAllUsersAvatarsFromDatabase(data) {
        for (var i = 0; i < data.length; i++) {
            if (! (data[i].nickname in usersAvatars) ){
                downloadImage(data[i].nickname);
            }
        }
        HomeService.usersHashMap = usersAvatars;
    }

    function downloadImage(nickname) {
        var url = 'http://people-around-you-frog0.c9users.io:8080/getImage?image=' + nickname + '.png';
        var filename = nickname + '.png',
        targetPath = cordova.file.externalRootDirectory + '/peopleAroundYou/' + filename,
        options = {},
        trustHosts = true;
        $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
        .then(
            function(result) {
                addUserToHashMapAndServiceList(nickname, result);
            },
            function(err) {
              alert('Error: ' + JSON.stringify(err));
            },
            function(progress) {
            }
        );
    }

    function addUserToHashMapAndServiceList(nickname, result) {
        usersAvatars[nickname] = result.toURL();
        if ( nickname.localeCompare(ownerInfo.nickname) !== 0){
            var lista = HomeService.usersList;
            lista.push({nickname: nickname, avatar: result.toURL()});
            HomeService.usersList = lista;
        }
    }


    function emptyTheUsersArray() {
        for (var i = 0; i <= users.length; i++) {
            users.pop();
        }
    }

   //UPDATE POSITIONS----------------------
   function getPositionsCoordinates(position) {
       $scope.$apply(function() {
           var lati = position.coords.latitude;
           var longi = position.coords.longitude;
           ownerInfo.lati = lati;
           ownerInfo.longi = longi;
           HomeService.setOwnerInfo(ownerInfo);
           owner.lati = lati;
           owner.longi = longi;
           getNewPositions();
      });
   }

   function getNewPositions() {
       for (var i = 0; i < users.length; i++) {
           getUserPositionBasedOnCoords(users[i]);
      }
   }

   function getUserPositionBasedOnCoords(user) {
       //get actual longi/lati distances in meters
       var actualLongiDist = distanceBtwnTwoPoints(owner.lati, owner.longi, owner.lati, user.longi);//same lati, different longi
       var actualLatiDist = distanceBtwnTwoPoints(owner.lati, owner.longi, user.lati, owner.longi);//same longi, different lati
       /*translate it to pixel distance for the RADAR, with the RULE OF THREE.
       The actual radious is 50-500 meters but the radar radious is screen.width/2, usually 150-180 PX*/
       var radarRadious = CANVAS_SIDE/2; //pixels
       var actualRadious = RADAR_RADIUS; //meters
       var pixelLongiDist = (actualLongiDist * radarRadious) / actualRadious;
       var pixelLatiDist = (actualLatiDist * radarRadious) / actualRadious;
       latiDifference = user.lati - owner.lati;
       longiDifference = user.longi - owner.longi;
       if( latiDifference >= 0) {//above owner
           user.centerY = owner.centerY - Math.floor(pixelLatiDist);
       }
       else {// bellow owner
           user.centerY = owner.centerY + Math.floor(pixelLatiDist);
       }
       if ( longiDifference >= 0 ) {// right of owner`
           user.centerX = owner.centerX + Math.floor(pixelLongiDist);
       }
       else { // left of owner
           user.centerX = owner.centerX - Math.floor(pixelLongiDist);
       }
       updateUserCoords(user);
   }

   function updateUserCoords(user) {
       user.x = user.centerX - (user.side/2);
       user.y = user.centerY - (user.side/2);
       user.cartX = user.centerX - (CANVAS_SIDE/2);
       user.cartY = (CANVAS_SIDE/2) - user.centerY;
       user.cartHypotenuse = Math.sqrt((user.cartX * user.cartX) + (user.cartY * user.cartY));
       user.cartesianAngle = angleOfPoint(user.centerX, user.centerY);
   }

    //COMPASS functions
    function headSuccess(heading) {
        $scope.$apply(function() {
        	headingAngle = heading.magneticHeading;
            $scope.magHead = headingAngle;
            $scope.headTimestamp = heading.timestamp;
            getUsersDrawingPositions(headingAngle);
        });
    }

    function getUsersDrawingPositions(angle) {
        for (var i = 0; i < users.length; i++) {
            updateUserDrawingPosition(angle, users[i]);
        }
    }

    function updateUserDrawingPosition(compassAngle, user) {

        var totalAngle = user.cartesianAngle + compassAngle;
        //set back to 0 when it passes 360
        if ( totalAngle > 360) {
            totalAngle -= 360;
        }
        //cartesian position of center. We have new position as the compass angle has changed
        //hypotenuse remains constant as it's the radius of the circle between owner and user (with owner the center)
        var cartX = Math.cos(degreesToRadians(totalAngle)) * user.cartHypotenuse;
        var cartY = Math.sin(degreesToRadians(totalAngle)) * user.cartHypotenuse;
        // actual position of center. Positions to draw (with (0, 0) the top left corner of the canvas).
        var actualX = Math.floor(cartX + CANVAS_SIDE/2);
        var actualY = Math.floor(CANVAS_SIDE/2 -cartY);
        // -user.side/2 cause we need to draw from the top left corner and not the center.
        user.drawX = actualX - (user.side/2);
        user.drawY = actualY - (user.side/2);
        // ctx.drawImage(user.image, actualX - (user.side/2), actualY - (user.side/2), user.side, user.side);
    }

    function headError(error) {
    	$scope.$apply(function() {
    	 	$scope.headError = error.code;
    	});
    }

    /*Credit to "Movable Type Scripts". A really good web site for geolocation calculation.
      http://www.movable-type.co.uk/scripts/latlong.html */
    function distanceBtwnTwoPoints(lati1, longi1, lati2, longi2) {
        var R = 6371e3; // metres
        var φ1 = degreesToRadians(lati1);
        var φ2 = degreesToRadians(lati2);
        var Δφ = degreesToRadians((lati2-lati1));
        var Δλ = degreesToRadians((longi2-longi1));
        var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ/2) * Math.sin(Δλ/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        //returns the distance between two spots in meters.
        return d;
    }

    function degreesToRadians(Value) {
	    // Converts and returns numeric degrees to radians
	    return Value * Math.PI / 180;
	}

    function radiansToDegrees(radians) {
        return (radians * 180) / Math.PI;
    }


    // CHAT FUNCTIONS

    // the "send message button" function
    $scope.sendMessage = function () {
        var textArea = $window.document.getElementById('homeMessage');
        var element = {author: ownerInfo.nickname, message: $scope.data.message};
        //don't want to do anything if there is no user selected. (won't know where to send the message)
        if($scope.data.conversation === -2 ) {
            if (textArea) {
                textArea.value = '';
            }
            return;
        }
        $scope.data.author = ownerInfo.nickname;
        $scope.data.$save(function(response) {
            if(response.answer.localeCompare('fail') === 0) {
                alert("The message wasn't sent. Please try again");
            }
            else {
                socket.emit('send message', element);
                if(response.conversation !== undefined) {
                    $scope.data.conversation = response.conversation;
                }
            }
        });
        $timeout(function() {
            viewScroll.scrollBottom();
            if(textArea) {
                if(textArea.value.localeCompare('') !== 0) {
                    $scope.message = textArea.value;
                    textArea.value = '' ;
                }
                textArea.focus();
            }
            viewScroll.scrollBottom();
        }, 500);
    };

    //when the keyboard from the bottom appears it doesn't overlap the application
    $scope.focusFunction = function() {
        $timeout(function () {
            console.log("ti timeout activated");
            viewScroll.scrollBottom();
        }, 500);
    }

})

.controller('profileCtrl', function($scope, HomeService) {

    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    $scope.$on('$ionicView.enter', function(event, data) {
        ownerInfo = HomeService.getOwnerInfo();
    });
    var menuAvatar = document.getElementById('profile-page-avatar');
    menuAvatar.src = ownerInfo.avatar;
    $scope.name = ownerInfo.name;
    $scope.surname = ownerInfo.surname;
    $scope.nickname = ownerInfo.nickname;
    $scope.phone = ownerInfo.phone;
    $scope.DOB = ownerInfo.DOB;
    $scope.status = ownerInfo.status;
    $scope.language = ownerInfo.language;
})

.controller('profilePhotoCtrl', function($scope, HomeService, $ionicPopup ) {


    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    var menuAvatar = document.getElementById('profilePhoto-image');
    menuAvatar.src = ownerInfo.avatar;

    $scope.save = function() {
        var imagePath = document.getElementById('profilePhoto-image').src;
        ownerInfo.avatar = imagePath;
        HomeService.setOwnerInfo(ownerInfo);
        var temp2 = new HomeService.OwnerInfo();
        temp2 = HomeService.getOwnerInfo();
    };

    $scope.checkAvatar = function(){
        var temp = new HomeService.OwnerInfo();
        temp = HomeService.getOwnerInfo();
    };

    $scope.changePhoto = function () {
        var myPopup = $ionicPopup.show({
            title: 'Add image from:',
            scope: $scope,
            buttons: [
            {
            text: 'Camera',
            type: 'button-calm',
            onTap: function(e) {
              console.log("camera");
              fromCamera();
            }
            },
            {
            text: 'Gallery',
            type: 'button-calm',
            onTap: function(e) {
              console.log("Gallery");
              fromGallery();
            }
            }]
        });
    };

    function fromCamera() {
        var cameraOptions = {quality: 80,
                             correctOrientation: true,
                             targetWidth: 600,
                             encodingType: navigator.camera.EncodingType.JPEG};
        navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
    }

    function fromGallery() {
        var cameraOptions = {quality: 80,
                             correctOrientation: true,
                             targetWidth: 600,
                             sourceType: navigator.camera.PictureSourceType.SAVEDPHOTOALBUM};
        navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
    }

    function cameraSuccess(uri) {
        var image = document.getElementById("profilePhoto-image");
        image.src = uri;
    }

    function cameraError(message) {
        console.log("error: " + message);
    }

})

.controller('loginCtrl', function($scope, LoginService, $state, $timeout, HomeService, $cordovaFileTransfer, $ionicLoading) {
    //by default the login form and spinner are hidden
    $scope.reveal = false;
    $scope.spinner = false;

    var jason = {message: 'test message', author: 'test author'}
    var socket = io("http://people-around-you-frog0.c9users.io:8080");
    socket.emit('test socket', jason);
    socket.on('test socket', function(message) {
        console.log('autoo einai to return apo to socket kai to mhnuma einai: ' + message);
    })

    //load an ownerInfo object from the service that will hold all of the user's information.
    var owner = new HomeService.OwnerInfo();
    HomeService.setOwnerInfo(owner);
    var result = HomeService.getOwnerInfo();
    $scope.test = result;

    //create a new $resource object. AKA connection with the server.
    $scope.data = new LoginService.connectLogin();


    $scope.loginSubmit = function() {
        var loginForm = document.getElementById("login-button1");
        $timeout(function() {
            angular.element(loginForm).triggerHandler('click');
        }, 0);
        $scope.spinner = true;
        $scope.data.$save(function(response) {
            console.log(response);
            if(response.nickname !== undefined){
                if(response.nickname.localeCompare("") === 0 ) {
                    $scope.spinner = false;
                    LoginService.alertPopup("Can't login. Make sure the email and password are correct!");
                }
                else {//found a user and we can login
                    //give his information to the service to use it to the other views as well*/
                    var owner = new HomeService.OwnerInfo();
                    owner = HomeService.getOwnerInfo();
                    putInfoToOwner(response);
                    downloadAvatarAndGoToHome(response.avatar);
                }
            }
            else{
                LoginService.alertPopup('Something went wrong. Try again.');
            }
        });
    };

    function putInfoToOwner(response) {
        owner.email = response.email;
        owner.nickname = response.nickname;
        owner.name = response.name;
        owner.surname = response.surname;
        owner.status = response.status;
        owner.DOB = response.date_of_birth;
        owner.phone = response.phone;
        owner.language = response.language;
        HomeService.setOwnerInfo(owner);
    }

    function downloadAvatarAndGoToHome(avatar){
        downloadImage(avatar, function() {
            $state.go('menu.home');
        });
    }

    function downloadImage(avatarName, callback) {
        var url = 'http://people-around-you-frog0.c9users.io:8080/getImage?image=' + avatarName;
        var filename = url.split('/').pop,
        targetPath = cordova.file.externalRootDirectory + filename,
        options = {},
        trustHosts = true;
        $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
        .then(
            function(result) {
                $ionicLoading.hide();
                var temp = new HomeService.OwnerInfo();
                temp = HomeService.getOwnerInfo();
                temp.avatar = result.toURL();
                HomeService.setOwnerInfo(temp);
                callback();
            },
            function(err) {
              alert('Error: ' + JSON.stringify(err));
            },
            function(progress) {
            }
        );
    }


})

.controller('signupCtrl', function($scope, socialProvider, LoginService, $state, $timeout, HomeService, $ionicLoading) {

    //by default the login form is hidden
    $scope.reveal = false;
    $scope.spinner = false;
    //create a new $resource object.
    $scope.data = new LoginService.connectSignup();

    $scope.signupSubmit = function() {
        // if(isEmailAllowed()) {
            $scope.spinner = true;
            var signupForm = document.getElementById("signup-button3")
            $timeout(function() {
                angular.element(signupForm).triggerHandler('click');
            }, 0);
            LoginService.setNickname($scope.data.nickname);
            $scope.data.$save(function(response) {
                if(response.answer.localeCompare("exists") === 0) {
                    $scope.spinner = false;
                    LoginService.alertPopup("User already exists with that email or nickname.");
                }
                else if (response.answer.localeCompare("fail") === 0){
                    $scope.spinner = false;
                    LoginService.alertPopup("Signup failed. Please try again.");
                }
                else{
                    var owner = new HomeService.OwnerInfo();
                    owner.email = response.email;
                    owner.nickname = response.nickname;
                    HomeService.setOwnerInfo(owner);
                    $state.go('activationFlow');
                }
            });
        // }
    };

    function isEmailAllowed(){
        //accept only educational email domains
        if(!LoginService.ends($scope.data.email, ".ac.uk") &&
           !LoginService.ends($scope.data.email, ".edu") ) {
               LoginService.alertPopup("You need to have an educational email to enter.");
               return false;
           }
           else{ return true; }
    }

})

.controller('settingsCtrl', function($scope, $timeout, $ionicPlatform, LoginService, servSettings, HomeService) {

    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    var menuAvatar = document.getElementById('settingsAvatar');
    settingsAvatar.src = ownerInfo.avatar;
    $scope.nickname = ownerInfo.nickname;
    $scope.status = ownerInfo.status;

        //-----------RADAR RADIUS
    //get the value of the radius range input
	var sliderNewValue = document.getElementById("radius").value;
	// storing the value in the services' localStorage to be used by the home controller
	updateRadius(sliderNewValue);

    //update the radius value when the HTML drag div changes
    $scope.dragRange = function(value) {
		updateRadius(value);
	};

	function updateRadius(value) {
        ownerInfo.radius = value;
        $timeout(function () {
            $scope.$apply(function() {
                HomeService.setOwnerInfo(ownerInfo);
                console.log(ownerInfo.radius);
            });
        }, 500);
		$scope.valueText = ownerInfo.radius;
	}

    //------FRAME RATE
    var rate = document.getElementById("FPS").value;
    //$scope.rate = rate;
    updateFPS(rate);
    $scope.fpsChange = function(value) {
        updateFPS(value);
    };

    function updateFPS(rate) {
        var speed;
        if( rate == 1) {
            speed = "slow";
        }
        else if (rate == 2 ) {
            speed = "medium";
        }
        else if(rate == 3 ) {
            speed = "fast";
        }
        $scope.debuging = rate;
        ownerInfo.frame = speed;
        HomeService.setOwnerInfo(ownerInfo);
        $scope.rate = ownerInfo.frame;
    }

    //sets the owner's position to null in the database and kills the app
    $scope.logOut = function() {
        var result = LoginService.connectSettings.get({nickname: ownerInfo.nickname});
        result.$promise.then(function(data) {
            if(data.answer.localeCompare('fail') === 0 ) {
                alert('Something went wrong. Please try again.');
            }
            else{
                ionic.Platform.exitApp();
            }
        });
    }

})

.controller('chatsCtrl', function($scope, LoginService, HomeService) {

    var result = LoginService.connectChats.query();

    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    HomeService.setOwnerInfo(ownerInfo);

    $scope.$on('$ionicView.enter', function(event, data) {
        var listes = HomeService.usersList;
        var screenText = document.getElementById('emptyChats');
        if (listes.length === 0)  {
            screenText.innerHTML = "There are no nearby users to chat with.";
        }
        else {
            screenText.innerHTML = "";
            for (var i = 0; i < listes.length; i++) {
                $scope.nickname = listes[i].nickname;
                $scope.avatar = listes[i].avatar;
            }
        }
        $scope.lista = listes;
    });

})

.controller('topicsCtrl', function($scope, $ionicPopup, $ionicScrollDelegate,
                                   SharedInfo, LoginService, $ionicLoading, HomeService, $timeout) {

    //take control of the view scroller
    var viewScroll = $ionicScrollDelegate.$getByHandle('topicsScroll');
    // HomeService.usersHashMap = {lol: 'lol.png', vlasis: 'vlasis.png'};
    var firstTime = true;
    var screenText = document.getElementById('emptyTopics');
    $scope.data = new LoginService.connectTopics();
    var ownerinfo = new HomeService.OwnerInfo();
    ownerinfo = HomeService.getOwnerInfo();

    /*socket that will allow to live update incoming messages (socket.io)
     http://socket.io/download/ */
    var socket = io("http://people-around-you-frog0.c9users.io:8080");
    socket.on('new topic', function(info) {
        $scope.$apply(function() {
            loadTopicsList();
        });
    });

    $scope.$on('$ionicView.enter', function( event, data) {
        $scope.reveal = false;
        $scope.spinner = false;
        loadTopicsList();
    });

    function loadTopicsList() {
        var result = LoginService.connectTopics.query();
        if (firstTime) {
            $ionicLoading.show();
            firstTime = false;
        }
        result.$promise.then(function(data) {
            var nameToAvatarMap = HomeService.usersHashMap;
            var finalData = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].author in nameToAvatarMap) {
                    finalData.push(data[i]);
                }
            }
            if (finalData.length == 0) {
                screenText.innerHTML = "There are no topics currently to display. Why not create one?"
            }
            else {
                screenText.innerHTML = ""
                $scope.userToAvatarMap = nameToAvatarMap;
                $scope.topics = finalData;
            }
            $ionicLoading.hide();
        });
    }

    $scope.displayInputField = function() {
        $scope.reveal = !$scope.reveal;
        viewScroll.scrollBottom();
    }

    $scope.showFullTitle = function(title){
        var alertPopup = $ionicPopup.alert({
            title: 'Full title',
            template: title,
            okText: 'dismiss'
        });
    }

    $scope.createTopicForm = function() {
        var topicButton = document.getElementById("createTopic-button1");
        $timeout(function() {
            angular.element(topicButton).triggerHandler('click');
        }, 0);
        $scope.spinner = true;
        $scope.data.author = ownerinfo.nickname;
        $scope.data.$save(function(response) {
            if (response.answer.localeCompare("inserted") === 0) {
                socket.emit('new topic', 'new');
                alert("New topic was successfully created");
                firstTime = true;
            }
            else{
                alert("something went wrong. Please try again");
            }
            $scope.spinner = false;
        });
    };

    $scope.storeTitle = function(title) {
        SharedInfo.topicTitle = title;
    }

})

.controller( 'topicDetails', function($scope, $timeout, $window, SharedInfo, $stateParams, HomeService, LoginService,  $ionicLoading, $ionicScrollDelegate) {

    //take control of the view scroller
    var viewScroll = $ionicScrollDelegate.$getByHandle('topicsScroll');

    var firstTime = true;
    var screenText = document.getElementById('noPosts');
    var ownerinfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    var intervalActive = false;
    var previusDataSize = 0;

    /*socket that will allow to live update incoming messages (socket.io)
     http://socket.io/download/ */
    var socket = io("http://people-around-you-frog0.c9users.io:8080");
    socket.on('new post', function(info) {
        $scope.$apply(function() {
            $scope.posts.push(info);
            viewScroll.scrollBottom();
        });
    });

    //$scopes that will be used in the view
    $scope.owner = ownerInfo;
    $scope.nameToAvatarMap = HomeService.usersHashMap;
    $scope.title = SharedInfo.topicTitle;
    $scope.data = new LoginService.connectTopicDetails() ;

    $scope.$on('$ionicView.enter', function(event, data) {
        fetchMessagesOfTopic();
    });

    function fetchMessagesOfTopic() {
        if (firstTime) {
            $ionicLoading.show();
            firstTime = false;
        }
        var result = LoginService.connectTopics.query({topicId: $stateParams.topicId});
        result.$promise.then(function(data) {
            if (data.length === 0) {
                screenText.innerHTML = "There are no posts in this topic. Why not send the first?"
            }
            else {
                screenText.innerHTML = "";
                $scope.posts = data;
            }
            $ionicLoading.hide();
            if(previusDataSize !== data.length) {
                viewScroll.scrollBottom();
            }
            previusDataSize = data.length;
        });
    }

    $scope.sendPost = function() {
        var element = {author: ownerInfo.nickname , message: $scope.data.text}
        $scope.data.author = ownerInfo.nickname;
        $scope.data.topicId = $stateParams.topicId;
        $scope.data.$save(function(response) {
            if (response.answer.localeCompare("fail") === 0 ) {
                alert("The post wasn't saved. Please try again.");
            }
            else {
                socket.emit('new post', element);
            }
        });
        $timeout(function () {
            var textArea = $window.document.getElementById('postTextarea');
            if(textArea) {
                textArea.focus();
                viewScroll.scrollBottom();
            }
        });
    }

    //when the keyboard from the bottom appears it doesn't overlap the application
    $scope.focusFunction = function() {
        $timeout(function () {
            viewScroll.scrollBottom();
        }, 500);
    }


})

.controller('chatDetailsCtrl', function($scope, $window, $timeout, $stateParams, HomeService, LoginService, $ionicLoading, $ionicScrollDelegate) {

    //take control of the view scroller
    var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');

    //get the avatar path of the user (not the owner)
    var userName = $stateParams.nickname;
    var userAvatar = HomeService.usersHashMap[userName];

    //get info of the owner
    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    var screenText = document.getElementById('noMessages');

    //scopes with information for our chat detail view.
    $scope.owner = ownerInfo;
    $scope.nickname = userName
    $scope.userAvatar = userAvatar;
    $scope.data = new LoginService.connectChatDetails() ;
    $scope.messates = {};

    /*socket that will allow to live update incoming messages (socket.io)
     http://socket.io/download/ */
    var socket = io("http://people-around-you-frog0.c9users.io:8080");
    socket.on('send message', function(info) {
        $scope.$apply(function() {
            $scope.messages.push(info);
            viewScroll.scrollBottom();
        });
    });

    $scope.$on('$ionicView.enter', function(event, data) {
        fetchMessagesWithThatPerson();
    });

    function fetchMessagesWithThatPerson() {
        //set loader to give time to the app to fetch the messages
        $ionicLoading.show();
        var result = LoginService.connectChats.query({nickname: $stateParams.nickname, owner: ownerInfo.nickname});
        result.$promise.then(function(data) {
            $scope.dataSize = data.length;
            if (data.length === 0)  {
                /*mark the conversation as -1 so the server knows there is no active
                conversation with this person. This saves up one SQL query on the server*/
                $scope.data.conversation = -1;
                $scope.messages = data;
                screenText.innerHTML = "You haven't start a conversation with this person. Why not send the first message?";
            }
            else{
                $scope.data.conversation = data[0].conversation;
                noMessages = false;
                screenText.innerHTML = "";
                $scope.messages = data;
            }
            //after fetching the messages dismiss the loader.
            $ionicLoading.hide();
            viewScroll.scrollBottom();
        });
    }

    $scope.focusFunction = function() {
        $timeout(function () {
            viewScroll.scrollBottom();
        }, 500);
    }

    $scope.sendPrivateMessage = function() {
        var element = {author: ownerInfo.nickname , message: $scope.data.message};
        $scope.data.author = ownerInfo.nickname;
        $scope.data.user = $stateParams.nickname;
        $scope.data.$save(function(response) {
            if (response.answer.localeCompare("fail") === 0) {
                alert("The message wasn't sent. Please try again");
            }
            else {
                socket.emit('send message', element);
                if (response.conversation !== undefined) {
                    $scope.data.conversation = response.conversation;
                }
            }
            viewScroll.scrollBottom();
        });
        if(noMessages) {
            noMessages = false;
            screenText.innerHTML = "";
        }
        $timeout(function () {
            var textArea = $window.document.getElementById('chatMessage');
            if(textArea) {
                textArea.focus();
                viewScroll.scrollBottom();
            }
        });
    }

})

.controller('menuCtrl', function($scope, HomeService) {

    var ownerInfo = new HomeService.OwnerInfo();
    ownerInfo = HomeService.getOwnerInfo();
    $scope.$on('$ionicView.enter', function(event, data) {
        ownerInfo = HomeService.getOwnerInfo();
    });
    var menuAvatar = document.getElementById('side-menu-avatar');
    menuAvatar.src = ownerInfo.avatar;
    $scope.nickname = ownerInfo.nickname;
    $scope.status = ownerInfo.status;
})

.controller('activationFlowCtrl', function($scope, $window, $ionicPopup, LoginService,
                                           $cordovaFileTransfer, $state, HomeService) {


    //will also add the information on the service.
    var ownerInfo = new HomeService.OwnerInfo() ;
    ownerInfo = HomeService.getOwnerInfo();

    //bind $scope.data with $resource AKA server.
    $scope.data = new LoginService.connectProfile();
    $scope.data.nickname = LoginService.getNickname();
    $scope.data.avatar = ""; //will eventually be nickname.png, but it will be done on the server side

    $scope.profileInfo = function() {
        setFavouriteLanguage();
        upload();
        addInfoToOwnerInfo();
        $scope.data.$save(function(response) {
            if(response.answer.localeCompare('fail') === 0 ) {
                alert('something went wrong with the database insertion');
            }
            else if (response.answer.localeCompare('inserted') === 0 ) {
                addAvatarToOwnerInfo();
                $state.go('menu.home');
            }
        });
    };

    function addInfoToOwnerInfo() {
        var temp = new HomeService.OwnerInfo();
        temp = HomeService.getOwnerInfo();
        temp.name = $scope.data.firstName;
        temp.surname = $scope.data.lastName;
        temp.status = $scope.data.status;
        temp.phone = $scope.data.phone;
        temp.DOB = $scope.data.date;
        temp.language = $scope.data.languageSelect;
        HomeService.setOwnerInfo(temp);
    }

    function upload() {
        var x = 1;
        var options = {
            fileKey: 'vlasis', //email here
            fileName: LoginService.getNickname(), //and here email.png
            chunkedMode: "false",
            mineType: "image/png"
        };
        var trustHosts = true;
        var url = 'http://people-around-you-frog0.c9users.io:8080/uploadImage';
        var storePath = document.getElementById('avatarIcon').src;
        $scope.image = storePath;
        $cordovaFileTransfer.upload(url, storePath, options, trustHosts).then( function(result) {
            $scope.image = "SUCCESS!!!!";
        }, function(error) {
            alert('Error: ' + JSON.stringify(error));
        }, function(progress) {
            //currently doing nothing
        });
    }

    function setFavouriteLanguage() {
        if($scope.data.languageSelect != undefined) {
            if($scope.data.languageSelect.localeCompare("other") === 0 ) {
                if ($scope.data.languageInput == undefined ) {
                    $scope.data.languageInput = "";
                }
                $scope.data.languageSelect = $scope.data.languageInput;
            }
        }
    }

    function addAvatarToOwnerInfo() {
        var avatarPath = document.getElementById('avatarIcon').src;
        var ownerInfo = new HomeService.OwnerInfo();
        ownerInfo = HomeService.getOwnerInfo();
        ownerInfo.avatar = avatarPath;
        HomeService.setOwnerInfo(ownerInfo);
        return ownerInfo.avatar;
    }

    $scope.changeAvatarPhoto = function () {
        var myPopup = $ionicPopup.show({
            title: 'Add image from:',
            scope: $scope,
            buttons: [
            {
            text: 'Camera',
            type: 'button-calm',
            onTap: function(e) {
              console.log("camera");
              fromCamera();
            }
            },
            {
            text: 'Gallery',
            type: 'button-calm',
            onTap: function(e) {
              console.log("Gallery");
              fromGallery();
            }
            }]
        });
    };

    function fromCamera() {
        var cameraOptions = {quality: 80,
                             correctOrientation: true,
                             targetWidth: 600,
                             encodingType: navigator.camera.EncodingType.JPEG};
        navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
    }

    function fromGallery() {
        var cameraOptions = {quality: 80,
                             correctOrientation: true,
                             targetWidth: 600,
                             sourceType: navigator.camera.PictureSourceType.SAVEDPHOTOALBUM};
        navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
    }

    function cameraSuccess(uri) {
        var image = document.getElementById("avatarIcon");
        $scope.image = uri;
        image.src = uri;
    }

    function cameraError(message) {
        console.log("error: " + message);
    }

    //reveal an input field to enter your custom language when "other" is selected. Hidden by default
    $scope.otherOptions = false;
    $scope.showSelectedValue = function(value) {
        if(value.localeCompare("other") === 0 ) {
            $scope.otherOptions = true;
        }
        else {
            $scope.otherOptions = false;
        }
    };

});
