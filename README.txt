* This application was my summer individual project and was developed entirely by myself.
* After signing up, enter a nickname and an image/avatar. 
* The home page has a radar that will allow you to locate nearby users. By default the radius is 100 meters, but that can be changed from the settings page.
* Once other users are located, just tap on their avatar and start chatting with them.
* Swipe left to reveal the rest of the application's pages. 
* There is a private chat page, where users can chat directly with each other.
* Finally there is a Topic page to create interesting topics, where everyone can join for group chatting.